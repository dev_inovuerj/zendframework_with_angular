<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Application\Entity\Produtos;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;


class IndexController extends AbstractActionController
{

    public function indexAction()
    {

        /**
         * @global $em \Doctrine\ORM\EntityManager
         */
        $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');

        /**
         * @global repo \Doctrine\ORM\EntityRepository
         */
        $repo = $em->getRepository('Application\Entity\Categoria');


        /*instancia de entity categoria. */
//        $categoria = new Categoria();
//        $categoria->setNome('Laptops');

        /*Persistindo com Entity */
//        $em->persist($categoria); // prepare
//        $em->flush(); // gravar


        /**
         * Capturando Service Categoria criando em $Module->getServiceConfig()
         * @global $categoriaService \Application\Service\Categoria
         */
        $categoriaService = $this->serviceLocator->get('Application\Service\Categoria');

        /*Actions Service*/
//        $categoriaService->insert('Jesus');
//        $categoriaService->update(['id' => 5, 'nome' => 'Jesus Salvador']);
//        $categoriaService->delete(5);

        /*Categoria via find do repository */
        $categoria_test = $repo->find(1);

//        $produto = new Produtos();
//        $produto
//            ->setNome('Game a')
//            ->setDescricao('Melhor Game do Ano')
//            ->setCategoria($categoria_test);

        /*Entity Manager*/
//        $em->persist($produto);
//        $em->flush();


        /**
         * @global $serviceProdutos \Application\Service\Produto
         */
        $serviceProdutos = $this->getServiceLocator()->get('Application\Service\Produto');

        $serviceProdutos->insert(['nome' => 'Fé', 'categoriaId' => 5, 'descricao' => 'Deus é Fiél']);

        /* Todos as categorias */
        $categorias = $repo->findAll();


        return new ViewModel(['categorias' => $categorias]);
    }
}
