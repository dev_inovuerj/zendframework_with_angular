<?php
/**
 * Created by PhpStorm.
 * User: emilio
 * Date: 22/11/16
 * Time: 22:19
 */

namespace Application\Service;

use Application\Entity\Produto as ProdutoEntity;
use Application\Entity\Produtos;
use Doctrine\ORM\EntityManager;

class Produto
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * Produto constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }

    /**
     * @param array $data
     * @return ProdutoEntity
     */
    public function insert(array $data)
    {

        $categoriaEntityProxy = $this->em->getReference('Application\Entity\Categoria', $data['categoriaId']);

        $produtoEntity = new \Application\Entity\Produto();
        $produtoEntity
            ->setCategoria($categoriaEntityProxy)
            ->setNome($data['nome'])
            ->setDescricao($data['descricao']);

        $this->em->persist($produtoEntity);
        $this->em->flush();

        return $produtoEntity;


    }

    /**
     * @param array $data
     * @return ProdutoEntity
     */
    public function update(array $data)
    {

        /**
         * @global $categoriaEntityProxy
         */
        $categoriaEntityProxy = $this->em->getReference('Application\Entity\Categoria', $data['categoriaId']);

        /**
         * @global $produtoEntity \Application\Entity\Produto
         */
        $produtoEntity = $this->em->getReference('Application\Entity\Produto', $data['id']);

        $produtoEntity
            ->setCategoria($categoriaEntityProxy)
            ->setNome($data['nome'])
            ->setDescricao($data['descricao']);

        $this->em->persist($produtoEntity);

        $this->em->flush();

        return $produtoEntity;


    }

    /**
     * @param $id
     */
    public function delete($id)
    {
        $produtoEntity = $this->em->getReference('Application\Entity\Produto', $id);

        $this->em->remove($produtoEntity);

        $this->em->flush();


    }

}