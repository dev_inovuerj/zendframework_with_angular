<?php
/**
 * Created by PhpStorm.
 * User: emilio
 * Date: 22/11/16
 * Time: 22:19
 */

namespace Application\Service;

use Application\Entity\Categoria as CategoriaEntity;
use Doctrine\ORM\EntityManager;

class Categoria
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * Categoria constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }

    /**
     * @param $nome
     * @return CategoriaEntity
     */
    public function insert($nome)
    {

        $categoriaEntity = new CategoriaEntity();


        $categoriaEntity->setNome($nome);

        $this->em->persist($categoriaEntity);

        $this->em->flush();

        return $categoriaEntity;


    }

    /**
     * @param array $data
     * @return CategoriaEntity
     */
    public function update(array $data)
    {
        /**
         * @global $categoriaEntity \Application\Entity\Categoria
         */
        $categoriaEntity = $this->em->getReference('Application\Entity\Categoria', $data['id']);

        $categoriaEntity->setNome($data['nome']);

        $this->em->persist($categoriaEntity);

        $this->em->flush();

        return $categoriaEntity;


    }


    public function delete($id)
    {
        $categoriaEntity = $this->em->getReference('Application\Entity\Categoria', $id);

        $this->em->remove($categoriaEntity);

        $this->em->flush();



    }

}